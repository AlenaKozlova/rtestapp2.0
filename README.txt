# RubiusTestApp

�lient-server application for displaying and evaluating music tracks.

## Getting Started

1. Clone repository
2. run npm install
3. run update-database (in package manager console VS )

### Prerequisites

ASP.NET Core 2.0  

###Errors

In some cases, the following error may occur:

Module build failed: Error: Missing binding 
Node Sass could not find a binding for your current environment: Windows 64-bit with Node.js 8.x 
Found bindings for the following environments: - Windows 64-bit with Node.js 5.x

To fix:
1. Run command line with administrator rights from the project folder, 
2. Run the command: npm rebuild node-sass



