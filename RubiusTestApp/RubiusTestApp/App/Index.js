﻿import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import App from './App.vue'


Vue.use(Vuex)
Vue.config.productionTip = false

const treeNodeType = {
    musician: 0,
    album: 1
}

const store = new Vuex.Store({
    state: {
        tracks: [],
        selectedAlbumId: null
    },
    getters: {
        tracks: state => { return state.tracks }
    },
    mutations: {
        updateTracksTable(state, node) {
            if (node && node.depth === treeNodeType.album) {
                state.selectedAlbumId = node.id;
            }

            if (node && node.depth === treeNodeType.musician) {
                state.tracks = [];
                state.selectedAlbumId = null;
            }

            if (state.selectedAlbumId !== null) {
                axios.get(`api/tracks/${state.selectedAlbumId}`).then(response => {
                    state.tracks = response.data

                })
            }

        }
    }

});

new Vue({
    el: '#app',
    store: store,
    template: "<App/>",
    components: { App }
})