﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SpaServices.Webpack;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RubiusTestApp.Db;
using RubiusTestApp.Db.Implementations;
using RubiusTestApp.Db.Interfaces;
using DbContext = RubiusTestApp.Db.DbContext;

namespace RubiusTestApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }



        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            string connection = Configuration.GetConnectionString("DefaultConnection");

            services.AddDbContext<DbContext>(options =>
                options.UseSqlServer(connection));

            services.AddTransient<IMusicianRepository , MusicianRepository>();
            services.AddTransient<ITrackRepository, TrackRepository>();

        }


        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {


            app.UseMvc();
          

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
                {
                    HotModuleReplacement = true
                });
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
        }
    }
}
