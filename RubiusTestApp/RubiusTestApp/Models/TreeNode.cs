﻿using System;
using System.Collections.Generic;

namespace RubiusTestApp.Models
{
    /// <summary>
    /// Tree node
    /// </summary>
    public class TreeNode
    {
        /// <summary>
        /// Tree Item id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Tree Item name
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Tree node type
        /// </summary>
        public Type Type { get; set; }
        /// <summary>
        /// Children nodes
        /// </summary>
        public List<TreeNode> Children { get; set; }

    }
}
