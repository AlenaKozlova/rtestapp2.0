﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RubiusTestApp.Models
{
    /// <summary>
    /// Tree node types
    /// </summary>
    public enum Type
    {
        MusicianNode = 0,
        AlbumNode = 1
    };
}
