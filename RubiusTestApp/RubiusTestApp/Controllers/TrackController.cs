﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RubiusTestApp.Db.Interfaces;
using RubiusTestApp.Db.Models;

namespace RubiusTestApp.Controllers
{
    [Route("api/tracks")]

    public class TrackController : Controller
    {
        private readonly ITrackRepository _trackRepository;
        public TrackController(ITrackRepository trackrepository)
        {
            _trackRepository = trackrepository;
        }

        [HttpGet]
        public IList<Track> Get(int? albumId)
        {
            if (albumId.HasValue)
            {

            }
            return _trackRepository.GetAll();
        }

        [HttpGet]
        [Route("{albumId}")]
        public IList<Track> GetTracksFromAlbum(int albumId)
        {
            return _trackRepository.GetTracksByAlbumId(albumId);
        }

        [HttpPost]
        [Route("removeFromFavourites/{id}")]
        public void RemoveFromFavourites(int id)
        {
            _trackRepository.RemoveTrackFromFavorites(id);
        }

        [HttpPost]
        [Route("addToFavourites/{id}")]
        public void AddToFavourites(int id)
        {
            _trackRepository.AddTrackToFavorites(id);
        }

        [HttpPost]
        [Route("addToListened/{id}")]
        public void AddToListened(int id)
        {
            _trackRepository.AddTrackToListened(id);
        }

        [HttpPost]
        [Route("addToLiked/{id}")]
        public void AddToLiked(int id)
        {
            _trackRepository.AddTrackToLiked(id);
        }

        [HttpPost]
        [Route("addToDisliked/{id}")]
        public void AddToDisliked(int id)
        {
            _trackRepository.AddTrackToDisliked(id);
        }

        [HttpPost]
        [Route("{id}/setRating/{rating}")]
        public void SetRating(int id, int rating)
        {
            _trackRepository.SetRating(id, rating);
        }

    }
}