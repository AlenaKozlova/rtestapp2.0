﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using RubiusTestApp.Db.Interfaces;
using RubiusTestApp.Db.Models;
using RubiusTestApp.Models;

namespace RubiusTestApp.Controllers
{
    [Route("api/musicians")]
    public class MusicianController : Controller
    {
        private readonly IMusicianRepository _musicianRepository;

        public MusicianController(IMusicianRepository musicianrepository)
        {
            _musicianRepository = musicianrepository;
        }

        [HttpGet]
        public IList<Musician> Get()
        {
            return _musicianRepository.GetAll(false);
        }

        [HttpGet]
        [Route("tree")]
        public IList<TreeNode> BuildTree()
        {
            List<TreeNode> tree = null;
            var musicians = _musicianRepository.GetAll(true);
            if (musicians != null)
            {
                tree = new List<TreeNode>();
                foreach (var musician in musicians)
                {
                    List<TreeNode> albumnotes = null;
                    if (musician.Albums != null)
                    {
                        albumnotes = new List<TreeNode>();
                        foreach (var album in musician.Albums)
                            albumnotes.Add(new TreeNode()
                            {
                                Id = album.Id,
                                Text = album.Name,
                                Type = Type.AlbumNode
                            });
                    }

                    tree.Add(new TreeNode()
                    {
                        Id = musician.Id,
                        Text = musician.Name,
                        Type = Type.MusicianNode,
                        Children = albumnotes
                    });
                }
            }
            return tree;
        }
    }
}