﻿using Microsoft.EntityFrameworkCore;
using RubiusTestApp.Db.Models;

namespace RubiusTestApp.Db
{
    public class DbContext : Microsoft.EntityFrameworkCore.DbContext
    {

        public DbSet<Musician> Musicians { get; set; }

        public DbSet<Album> Albums { get; set; }

        public DbSet<Track> Tracks { get; set; }

        public DbContext(DbContextOptions<DbContext> options) : base(options)
        {
        }
    }
}
