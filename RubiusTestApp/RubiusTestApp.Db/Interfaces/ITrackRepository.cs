﻿using RubiusTestApp.Db.Models;
using System.Collections.Generic;

namespace RubiusTestApp.Db.Interfaces
{
    public interface ITrackRepository : IBaseRepository<Track>
    {
        /// <summary>
        /// The method for getting all tracks.
        /// </summary>
        /// <returns></returns>
        IList<Track> GetAll();

        /// <summary>
        /// Get all tracks of current album.
        /// </summary>
        /// <param name="albumId"></param>
        /// <returns></returns>
        IList<Track> GetTracksByAlbumId(int albumId);

        /// <summary>
        /// Add track to favorite tracks
        /// </summary>
        /// <param name="trackId">Track Id</param> 
        void AddTrackToFavorites(int trackId);

        /// <summary>
        /// Remove track from favourite tracks
        /// </summary>
        /// <param name="trackId"></param>
        void RemoveTrackFromFavorites(int trackId);

        /// <summary>
        /// Mark track as listened
        /// </summary>
        /// <param name="trackId">Track Id</param>
        void AddTrackToListened(int trackId);

        /// <summary>
        /// Add to track rating as the number of stars
        /// </summary>
        /// <param name="trackId"></param>
        /// <param name="rating"></param>
        void SetRating(int trackId, int rating);

        /// <summary>
        /// Mark track as liked
        /// </summary>
        /// <param name="trackid"></param>
        void AddTrackToLiked(int trackid);

        /// <summary>
        /// Mark track as disliked
        /// </summary>
        /// <param name="trackid"></param>
        void AddTrackToDisliked(int trackid);

    }
}
