﻿using RubiusTestApp.Db.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RubiusTestApp.Db.Interfaces
{
    public interface IAlbumRepository : IBaseRepository<Album>
    {
        /// <summary>
        /// The method for getting all albums.
        /// </summary>
        /// <returns></returns>
        IList<Album> GetAll();
    }
}