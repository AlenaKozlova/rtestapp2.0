﻿namespace RubiusTestApp.Db.Interfaces
{
    /// <summary>
    /// The interface for base entity
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IBaseRepository<T> where T : Models.BaseModel
    {

        /// <summary>
        /// Get entity by id.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <returns>Entity.</returns>
        T GetById(int id);

        /// <summary>
        /// Create new entity.
        /// </summary>
        /// <param name="newEntity">New entity.</param>
        /// <returns>Created entity.</returns>
        T Create(T newEntity);

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Updated entity.</param>
        void Update(T entity);

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="id">Id of deleted entity.</param>
        void Delete(int id);
    }
}
