﻿using System.Collections.Generic;
using RubiusTestApp.Db.Models;

namespace RubiusTestApp.Db.Interfaces
{
    public interface IMusicianRepository : IBaseRepository<Musician>
    {
        /// <summary>
        /// The method for getting all musicions.
        /// </summary>
        /// <returns></returns>
        IList<Musician> GetAll(bool IsAlbumsIncluded);
    }
}

