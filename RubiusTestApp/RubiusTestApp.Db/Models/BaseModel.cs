﻿namespace RubiusTestApp.Db.Models
{
    public class BaseModel
    {
        /// <summary>
        /// Base entity Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Base entity Name 
        /// </summary>
        public string Name { get; set; }
    }
}

