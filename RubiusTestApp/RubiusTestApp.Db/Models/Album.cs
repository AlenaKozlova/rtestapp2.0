﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace RubiusTestApp.Db.Models
{
    public class Album : BaseModel
    {
        /// <summary>
        /// Album release year
        /// </summary>
        public DateTime ReleaseYear { get; set; }
        /// <summary>
        /// Album track collection
        /// </summary>
        public virtual ICollection<Track> Tracks { get; set; }
        /// <summary>
        /// Musician Id
        /// </summary>
        [ForeignKey(nameof(MusicianId))]
        public int MusicianId { get; set; }
        /// <summary>
        /// Musician
        /// </summary>
        public virtual Musician Musician { get; set; }
    }
}
