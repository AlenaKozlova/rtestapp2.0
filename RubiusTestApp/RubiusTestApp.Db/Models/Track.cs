﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RubiusTestApp.Db.Models
{
    public class Track : BaseModel
    {
        /// <summary>
        /// Track duration
        /// </summary>
        public double Duration { get; set; }

        /// <summary>
        /// The track is added to favorites
        /// </summary>
        public bool IsFavorite { get; set; }

        /// <summary>
        /// The track is listened
        /// </summary>
        public bool IsListened { get; set; }

        /// <summary>
        /// Track rating
        /// </summary>
        public double Rating { get; set; }
        /// <summary>
        /// The track is liked
        /// </summary>
        public bool IsLiked { get; set; }
        /// <summary>
        /// Album
        /// </summary>
        [ForeignKey(nameof(AlbumId))]
        public int AlbumId { get; set; }
        /// <summary>
        /// Album 
        /// </summary>
        public virtual Album Album { get; set; }
    }
}
