﻿using System;
using System.Collections.Generic;

namespace RubiusTestApp.Db.Models
{
    public class Musician : BaseModel
    {
        /// <summary>
        /// The age of the musicion
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Music genre in which the musician works
        /// </summary>
        public string Genre { get; set; }

        /// <summary>
        /// The year in which the musician began his career
        /// </summary>
        public DateTime CarreerStartYear { get; set; }

        /// <summary>
        /// Musician albums collection
        /// </summary>
        public virtual ICollection<Album> Albums { get; set; }
    }
}
