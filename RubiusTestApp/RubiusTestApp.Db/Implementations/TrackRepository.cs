﻿using System;
using RubiusTestApp.Db.Interfaces;
using RubiusTestApp.Db.Models;
using System.Collections.Generic;
using System.Linq;

namespace RubiusTestApp.Db.Implementations
{
    /// <summary>
    /// Track repository.
    /// </summary>
    public class TrackRepository : BaseRepository<Track>, ITrackRepository
    {
        #region - Constructors -

        public TrackRepository(DbContext context) : base(context)
        {
        }

        #endregion

        #region - public Methods -

        /// <summary>
        /// Get all tracks.
        /// </summary>
        public IList<Track> GetAll()
        {
            return Context.Tracks.ToList();
        }

        /// <summary>
        /// Get all tracks of current album.
        /// </summary>
        /// <param name="albumId">Album Id</param>
        /// <returns></returns>
        public IList<Track> GetTracksByAlbumId(int albumId)
        {
            return Context.Tracks.Where(i => i.AlbumId == albumId).ToList();
        }

        /// <summary>
        /// Add track to favorite tracks
        /// </summary>
        /// <param name="trackId">Track Id</param>
        public void AddTrackToFavorites(int trackId)
        {
            var track = GetById(trackId);

            if (track == null)
            {
                throw new Exception("The track is not found");
            }

            track.IsFavorite = true;
            Context.SaveChanges();

        }

        /// <summary>
        /// Remove track from favorite tracks
        /// </summary>
        /// <param name="trackId"></param>
        public void RemoveTrackFromFavorites(int trackId)
        {
            var track = GetById(trackId);

            if (track == null)
            {
                throw new Exception("The track is not found");
            }

            track.IsFavorite = false;
            Context.SaveChanges();

        }

        /// <summary>
        /// Add to the list of listened tracks
        /// </summary>
        /// <param name="trackId"></param>
        public void AddTrackToListened(int trackId)
        {
            var track = GetById(trackId);

            if (track == null)
            {
                throw new Exception();
            }

            track.IsListened = true;
            Context.SaveChanges();

        }

        /// <summary>
        /// Set rating to track
        /// </summary>
        /// <param name="trackId"></param>
        /// <param name="rating"></param>
        public void SetRating(int trackId, int rating)
        {
            var track = GetById(trackId);
            if (track == null)
            {
                throw new Exception();
            }

            track.Rating = rating;
            Context.SaveChanges();

        }

        /// <summary>
        /// Add  track to the list of favorite tracks
        /// </summary>
        /// <param name="trackId"></param>
        public void AddTrackToLiked(int trackId)
        {
            var track = GetById(trackId);
            if (track == null)
            {
                throw new Exception();
            }

            track.IsLiked = true;
            Context.SaveChanges();
        }

        /// <summary>
        /// Remove track from the list of favorite tracks
        /// </summary>
        /// <param name="trackId"></param>
        public void AddTrackToDisliked(int trackId)
        {
            var track = GetById(trackId);
            if (track == null)
            {
                throw new Exception();
            }

            track.IsLiked = false;
            Context.SaveChanges();
        }

        #endregion
    }
}
