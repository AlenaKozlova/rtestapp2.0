﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RubiusTestApp.Db.Interfaces;
using RubiusTestApp.Db.Models;

namespace RubiusTestApp.Db.Implementations
{
    public class BaseRepository<T> : IBaseRepository<T> where T : BaseModel
    {

        protected readonly DbContext Context;

        protected BaseRepository(DbContext context)
        {
            Context = context;
        }

        #region Implementation of IRepository<T>

        /// <summary>
        /// Get entity by id.
        /// </summary>
        /// <param name="id">Identifier.</param>
        /// <returns>Entity.</returns>
        public T GetById(int id)
        {
            return Context.Set<T>().Find(id);
        }

        /// <summary>
        /// Create new entity.
        /// </summary>
        /// <param name="newEntity">New entity.</param>
        /// <returns>Created entity.</returns>
        public T Create(T newEntity)
        {
            Context.Set<T>().Add(newEntity);
            Context.SaveChanges();
            return newEntity;
        }

        /// <summary>
        /// Update entity.
        /// </summary>
        /// <param name="entity">Updated entity.</param>
        public void Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        /// <summary>
        /// Delete entity.
        /// </summary>
        /// <param name="id">Id of deleted entity.</param>
        public void Delete(int id)
        {
            var dbSet = Context.Set<T>();
            var entity = dbSet.Find(id);

            if (entity == null)
                throw new KeyNotFoundException();

            dbSet.Remove(entity);
            Context.SaveChanges();
        }
        #endregion
    }
}
