﻿using RubiusTestApp.Db.Interfaces;
using RubiusTestApp.Db.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace RubiusTestApp.Db.Implementations
{
    /// <summary>
    /// Musicion repository.
    /// </summary>
    public class MusicianRepository : BaseRepository<Musician>, IMusicianRepository
    {
        #region - Constructors -

        public MusicianRepository(DbContext context) : base(context)
        {
        }

        #endregion

        #region - public Methods -

        /// <summary>
        /// Get all musicians.
        /// </summary>
        public IList<Musician> GetAll(bool isAlbumIncluded)
        {
            if (isAlbumIncluded)
            {
                return Context.Musicians.Include(x => x.Albums).ToList();
            }

            return Context.Musicians.ToList();
        }
        #endregion
    }
}