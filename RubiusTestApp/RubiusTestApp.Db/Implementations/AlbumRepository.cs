﻿using RubiusTestApp.Db.Interfaces;
using RubiusTestApp.Db.Models;
using System.Collections.Generic;
using System.Linq;


namespace RubiusTestApp.Db.Implementations
{
    /// <summary>
    /// Album repository.
    /// </summary>
    public class AlbumRepository : BaseRepository<Album>, IAlbumRepository
    {
        #region - Constructors -

        public AlbumRepository(DbContext context) : base(context)
        {
        }

        #endregion

        #region - public Methods -

        /// <summary>
        /// Get all albums.
        /// </summary>
        public IList<Album> GetAll()
        {
            return Context.Albums.ToList();
        }

        #endregion
    }
}
