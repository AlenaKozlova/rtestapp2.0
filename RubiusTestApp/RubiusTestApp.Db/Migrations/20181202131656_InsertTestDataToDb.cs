﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RubiusTestApp.Db.Migrations
{
    public partial class InsertTestDataToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"

            SET IDENTITY_INSERT Musicians ON
            INSERT INTO Musicians (Id, Name, Age, Genre, CarreerStartYear)
                VALUES 
                (1, 'Edward Sheeran', '27', 'pop', '2006-01-01'),
                (2,'Ella Fitzgerald','27','jazz', '1942-01-01')

            SET IDENTITY_INSERT Musicians OFF 

            SET IDENTITY_INSERT [Albums] ON
            INSERT INTO [Albums] (Id, Name, ReleaseYear, MusicianId)
                VALUES 
                (1, 'Plus','2011-01-01',1),
                (2, 'Multiply','2014-01-01',1),
                (3, 'Devide','2017-01-01',1),   
                (4,'Ella Sings Gershwin', '1950-01-01', 2),
                (5,'Lullabies of Birdland', '1956-01-01', 2)
            SET IDENTITY_INSERT [Albums] OFF 

            SET IDENTITY_INSERT[Tracks] ON
                INSERT INTO[Tracks](Id, Name, Duration, IsFavorite, IsListened, IsLiked, Rating, AlbumId)
            VALUES
            (1, 'The A Team', 4.18, 0, 0, 0, 0, 1),
            (2, 'Drunk', 3.20, 0, 0, 0, 0, 1),
            (3, 'U.N.I.', 3.48, 0, 0, 0, 0, 1),
            (4, 'Grade 8', 2.59, 0, 0, 0, 0, 1),
            (5, 'One', 4.13, 0, 0, 0, 0, 2),
            (6, 'Im a Mess',4.06,0,0,0,0,2),
            (7, 'Nina', 3.43, 0, 0, 0, 0, 2),
            (8, 'Photograph', 4.17, 0, 0, 0, 0, 2),
            (9, 'Erazer', 3.47, 0, 0, 0, 0, 3),
            (10, 'Castle on the Hill', 4.20, 0, 0, 0, 0, 3),
            (11, 'Dive', 3.58, 0, 0, 0, 0, 3),
            (12, 'Shape of You', 3.53, 0, 0, 0, 0, 3),
            (13, 'Someone to Watch Over Me', 3.13, 0, 0, 0, 0, 4),
            (14, 'My One and Only', 3.13, 0, 0, 0, 0, 4),
            (15, 'But Not for Me', 3.12, 0, 0, 0, 0, 4),
            (16, 'Looking for a Boy', 3.06, 0, 0, 0, 0, 4),
            (17, 'Ive Got a Crush on You', 3.13, 0, 0, 0, 0, 4),
            (18, 'Maybe', 3.31, 0, 0, 0, 0, 4),
            (19, 'How Long Has This Been Going On?', 3.14, 0, 0, 0, 0, 4),
            (21, 'Lullaby of Birdland', 2.51, 0, 0, 0, 0, 5),
            (22, 'Rough Ridin', 3.14, 0, 0, 0, 0, 5),
            (23, 'Angel Eyes', 2.51, 0, 0, 0, 0, 5),
            (24, 'Oh Lady Be Good!', 3.08, 0, 0, 0, 0, 5),
            (25, 'Later', 2.32, 0, 0, 0, 0, 5)
            SET IDENTITY_INSERT[Tracks] OFF

            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
